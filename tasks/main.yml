---
- name: Create drop-in systemd config directory
  file:
    path: /etc/systemd/system/syslog-ng.service.d
    owner: root
    group: root
    mode: 0755
    state: directory

- name: Create drop-in systemd snippet for syslog-ng (wait for network)
  copy:
    dest: "/etc/systemd/system/syslog-ng.service.d/override.conf"
    content: |
      [Unit]
      After=network.target
    owner: root
    group: root
    mode: 0644
  notify:
    - Systemd daemon-reload
    - Restart syslog-ng

- name: Install syslog-ng package (RedHat)
  yum:
    name:
      - syslog-ng
      - syslog-ng-json
    state: "{{ syslog_ng_upgrade | ternary('latest','present') }}"
  when: ansible_facts['os_family'] == "RedHat"

- name: Install syslog-ng package (Debian)
  apt:
    name:
      - syslog-ng
    state: "{{ syslog_ng_upgrade | ternary('latest','present') }}"
  when: ansible_facts['os_family'] == "Debian"

- name: Install syslog-ng package (ESS Linux)
  command: "dnf --allowerasing -y install {{ item }}"
  args:
    warn: false
  with_items:
    - syslog-ng
  changed_when: false
  when: ansible_facts['os_family'] == "Concurrent CPU SDK"

- name: Deploy main config
  template:
    src: syslog-ng.conf.j2
    dest: /etc/syslog-ng/syslog-ng.conf
    mode: 0644
    owner: root
    group: root
  notify: Restart syslog-ng

- name: Create conf.d directory on ESS linux
  file:
    path: /etc/syslog-ng/conf.d
    owner: root
    group: root
    mode: 0755
    state: directory
  when: ansible_facts['os_family'] == "Concurrent CPU SDK"

- name: Create main spool directory (disk-buffer)
  file:
    path: "{{ syslog_ng_spool_dir }}"
    owner: root
    group: root
    mode: 0755
    state: directory

- name: Create spool sub directories
  file:
    path: "{{ syslog_ng_spool_dir }}/{{ item }}"
    owner: root
    group: root
    mode: 0755
    state: directory
  loop: "{{ syslog_ng_configs }}"

- name: Deploy defined custom configs
  template:
    dest: "/etc/syslog-ng/conf.d/{{ item }}.conf"
    src: "syslog-{{ item }}.j2"
    mode: 0644
    owner: root
    group: root
  loop: "{{ syslog_ng_configs }}"
  notify: Restart syslog-ng

- name: Flush handlers
  meta: flush_handlers

- name: Start and enable syslog-ng
  systemd:
    name: "{{ syslog_ng_service }}"
    state: started
    enabled: true

- name: Remove rsyslog
  yum:
    name: rsyslog
    state: absent
  when: syslog_ng_remove_rsyslog
