# ics-ans-role-syslog-ng

Ansible role to install syslog-ng.

## Requirements

- ansible >= 2.4
- molecule >= 2.6

## Role Variables

```yaml
syslog_ng_upgrade: false
syslog_ng_remove_rsyslog: true
syslog_ng_flush_lines: 0
syslog_ng_time_reopen: 10
syslog_ng_log_fifo_size: 1000
syslog_ng_chain_hostnames: 'off'
syslog_ng_use_dns: 'no'
syslog_ng_use_fqdn: 'no'
syslog_ng_create_dirs: 'no'
syslog_ng_keep_hostname: 'yes'
syslog_ng_enable_local_logs: true
syslog_ng_log_statistics: 0  # 0 to disable
syslog_ng_configs:
  - to_graylog
```

Please note the ```$$```: this is a required escape, otherwise the yaml parser will interpret ```$``` as a template

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-syslog-ng
```

## License

BSD 2-clause
