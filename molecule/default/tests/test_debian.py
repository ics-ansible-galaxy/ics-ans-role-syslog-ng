import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('debian_os_family')


def test_syslog_ng_is_running(host):
    sysng = host.service("syslog-ng")
    assert sysng.is_running
    assert sysng.is_enabled
